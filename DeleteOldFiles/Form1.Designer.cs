﻿namespace DeleteOldFiles
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderPicker = new System.Windows.Forms.FolderBrowserDialog();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.btnChooseFolder = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.txtStatus = new System.Windows.Forms.RichTextBox();
            this.dateTimerPicker = new System.Windows.Forms.DateTimePicker();
            this.btnDo = new System.Windows.Forms.Button();
            this.btnEsc = new System.Windows.Forms.Button();
            this.btnSaveDic = new System.Windows.Forms.Button();
            this.lblFilesCount = new System.Windows.Forms.Label();
            this.btnOpenDic = new System.Windows.Forms.Button();
            this.lblCountTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(12, 11);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(355, 20);
            this.txtPath.TabIndex = 0;
            // 
            // btnChooseFolder
            // 
            this.btnChooseFolder.Location = new System.Drawing.Point(373, 9);
            this.btnChooseFolder.Name = "btnChooseFolder";
            this.btnChooseFolder.Size = new System.Drawing.Size(91, 23);
            this.btnChooseFolder.TabIndex = 1;
            this.btnChooseFolder.Text = "Chọn thư mục";
            this.btnChooseFolder.UseVisualStyleBackColor = true;
            this.btnChooseFolder.Click += new System.EventHandler(this.btnChooseFolder_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 86);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(899, 23);
            this.progressBar.TabIndex = 2;
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(12, 115);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(899, 369);
            this.txtStatus.TabIndex = 3;
            this.txtStatus.Text = "";
            // 
            // dateTimerPicker
            // 
            this.dateTimerPicker.CustomFormat = "HH:mm:ss dd/MM/yyyy";
            this.dateTimerPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimerPicker.Location = new System.Drawing.Point(674, 11);
            this.dateTimerPicker.Name = "dateTimerPicker";
            this.dateTimerPicker.Size = new System.Drawing.Size(237, 20);
            this.dateTimerPicker.TabIndex = 4;
            this.dateTimerPicker.ValueChanged += new System.EventHandler(this.dateTimerPicker_ValueChanged);
            // 
            // btnDo
            // 
            this.btnDo.BackColor = System.Drawing.Color.OrangeRed;
            this.btnDo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDo.ForeColor = System.Drawing.Color.Transparent;
            this.btnDo.Location = new System.Drawing.Point(567, 38);
            this.btnDo.Name = "btnDo";
            this.btnDo.Size = new System.Drawing.Size(91, 41);
            this.btnDo.TabIndex = 1;
            this.btnDo.Text = "Thực hiện";
            this.btnDo.UseVisualStyleBackColor = false;
            this.btnDo.Click += new System.EventHandler(this.btnDo_Click);
            // 
            // btnEsc
            // 
            this.btnEsc.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnEsc.Location = new System.Drawing.Point(373, 38);
            this.btnEsc.Name = "btnEsc";
            this.btnEsc.Size = new System.Drawing.Size(91, 41);
            this.btnEsc.TabIndex = 1;
            this.btnEsc.Text = "Thoát";
            this.btnEsc.UseVisualStyleBackColor = true;
            this.btnEsc.Click += new System.EventHandler(this.btnEsc_Click);
            // 
            // btnSaveDic
            // 
            this.btnSaveDic.Location = new System.Drawing.Point(470, 9);
            this.btnSaveDic.Name = "btnSaveDic";
            this.btnSaveDic.Size = new System.Drawing.Size(91, 23);
            this.btnSaveDic.TabIndex = 1;
            this.btnSaveDic.Text = "Lưu thư mục";
            this.btnSaveDic.UseVisualStyleBackColor = true;
            this.btnSaveDic.Click += new System.EventHandler(this.btnSaveDic_Click);
            // 
            // lblFilesCount
            // 
            this.lblFilesCount.AutoSize = true;
            this.lblFilesCount.Location = new System.Drawing.Point(671, 66);
            this.lblFilesCount.Name = "lblFilesCount";
            this.lblFilesCount.Size = new System.Drawing.Size(75, 13);
            this.lblFilesCount.TabIndex = 5;
            this.lblFilesCount.Text = "Số file tìm thấy";
            this.lblFilesCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnOpenDic
            // 
            this.btnOpenDic.Location = new System.Drawing.Point(567, 9);
            this.btnOpenDic.Name = "btnOpenDic";
            this.btnOpenDic.Size = new System.Drawing.Size(91, 23);
            this.btnOpenDic.TabIndex = 1;
            this.btnOpenDic.Text = "Mở thư mục";
            this.btnOpenDic.UseVisualStyleBackColor = true;
            this.btnOpenDic.Click += new System.EventHandler(this.btnOpenDic_Click);
            // 
            // lblCountTotal
            // 
            this.lblCountTotal.AutoSize = true;
            this.lblCountTotal.Location = new System.Drawing.Point(671, 38);
            this.lblCountTotal.Name = "lblCountTotal";
            this.lblCountTotal.Size = new System.Drawing.Size(70, 13);
            this.lblCountTotal.TabIndex = 5;
            this.lblCountTotal.Text = "Tổng số files:";
            this.lblCountTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btnEsc;
            this.ClientSize = new System.Drawing.Size(923, 502);
            this.Controls.Add(this.lblCountTotal);
            this.Controls.Add(this.lblFilesCount);
            this.Controls.Add(this.dateTimerPicker);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnEsc);
            this.Controls.Add(this.btnDo);
            this.Controls.Add(this.btnOpenDic);
            this.Controls.Add(this.btnSaveDic);
            this.Controls.Add(this.btnChooseFolder);
            this.Controls.Add(this.txtPath);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Delete Old Files v0.1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderPicker;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Button btnChooseFolder;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.RichTextBox txtStatus;
        private System.Windows.Forms.DateTimePicker dateTimerPicker;
        private System.Windows.Forms.Button btnDo;
        private System.Windows.Forms.Button btnEsc;
        private System.Windows.Forms.Button btnSaveDic;
        private System.Windows.Forms.Label lblFilesCount;
        private System.Windows.Forms.Button btnOpenDic;
        private System.Windows.Forms.Label lblCountTotal;
    }
}

