﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeleteOldFiles
{
    public partial class Form1 : Form
    {
        static readonly string textFile = Path.Combine(Directory.GetCurrentDirectory(), "data.ini");

        public Form1()
        {
            InitializeComponent();
            var currentDic = getCurrentDicSave();
            if (!string.IsNullOrEmpty(currentDic))
            {
                txtPath.Text = currentDic;
                changeCountFiles(currentDic);
            }
        }

        private void btnChooseFolder_Click(object sender, EventArgs e)
        {
            folderPicker.SelectedPath = txtPath.Text;
            DialogResult result = folderPicker.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderPicker.SelectedPath))
            {
                //string[] folder = Directory.GetDirectories(folderPicker.SelectedPath);
                //string[] files = Directory.GetFiles(folderPicker.SelectedPath, "*", SearchOption.AllDirectories);
                txtPath.Text = folderPicker.SelectedPath;/* + " (" + folder.Length.ToString() + " folders - " + files.Length.ToString() + " files)";*/
                changeCountFiles(txtPath.Text);
            }
        }

        private void btnSaveDic_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPath.Text))
            {
                File.WriteAllText(textFile, txtPath.Text);
            }
        }
        private string getCurrentDicSave()
        {
            if (File.Exists(textFile))
            {
                string[] lines = File.ReadAllLines(textFile);
                if (lines.Length > 0 && !string.IsNullOrEmpty(lines[0]))
                    return lines[0];
            }
            return string.Empty;
        }

        private void btnDo_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPath.Text))
            {
                var confirmResult = MessageBox.Show("Are you sure to delete all files modified after " + dateTimerPicker.Value.ToString("HH:mm:ss dd/MM/yyyy") + "?", "Confirm Delete!!", MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    string[] files = Directory.GetFiles(txtPath.Text, "*", SearchOption.AllDirectories);
                    var dateDelete = dateTimerPicker.Value;

                    var fileRs = files.Where(c => File.GetLastWriteTime(c) <= dateDelete);
                    int totalFiles = fileRs.Count();
                    int iCount = fileRs.Count();
                    txtStatus.Text = "Bắt đầu xóa tập tin và thư mục";
                    foreach (var file in fileRs)
                    {
                        iCount -= 1;
                        lblFilesCount.Text = (totalFiles - iCount).ToString() + "/" + totalFiles.ToString() + " tập tin";
                        progressBar.Value = (totalFiles - iCount) * 100 / totalFiles;
                        File.Delete(file);
                        txtStatus.Text += "\n Đã xóa: " + file;
                    }

                    var rsEmptyFolder = DeleteEmptyFolder(txtPath.Text);
                    if (rsEmptyFolder != null && rsEmptyFolder.Count > 0)
                    {
                        foreach (var fd in rsEmptyFolder)
                        {
                            txtStatus.Text += "\n Đã thư mục rỗng: " + fd;
                        }
                    }

                    txtStatus.Text += "\nĐã xóa " + (totalFiles - iCount).ToString() + "/" + totalFiles.ToString() + " tập tin";
                    if (rsEmptyFolder != null)
                        txtStatus.Text += "\nĐã xóa " + rsEmptyFolder.Count.ToString() + "/" + rsEmptyFolder.Count.ToString() + " thư mục rỗng";
                    txtStatus.Text += "\nHoàn thành.";
                }
            }
        }

        private List<string> DeleteEmptyFolder(string startLocation)
        {
            var rs = new List<string>();
            foreach (var directory in Directory.GetDirectories(startLocation))
            {
                DeleteEmptyFolder(directory);
                if (Directory.GetFiles(directory).Length == 0 &&
                    Directory.GetDirectories(directory).Length == 0)
                {
                    rs.Add(directory);
                    Directory.Delete(directory, false);
                }
            }
            return rs;
        }

        private void dateTimerPicker_ValueChanged(object sender, EventArgs e)
        {
            changeCountFiles(txtPath.Text);
        }

        private void changeCountFiles(string currentDic)
        {
            string[] files = Directory.GetFiles(currentDic, "*", SearchOption.AllDirectories);
            var dateDelete = dateTimerPicker.Value;
            var fileRs = files.Where(c => File.GetLastWriteTime(c) <= dateDelete);
            lblFilesCount.Text = fileRs.Count().ToString() + "/" + fileRs.Count().ToString() + " files";
            lblCountTotal.Text = "Found: " + files.Count().ToString() + " files";
        }

        private void btnOpenDic_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPath.Text))
                Process.Start(txtPath.Text);
        }

        private void btnEsc_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
